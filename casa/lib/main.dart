import 'package:flutter/material.dart';
import 'package:casa/home.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        scaffoldBackgroundColor: Color.fromARGB(255, 255, 255, 255),
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.cyan,
        ).copyWith(
          secondary: Color.fromARGB(255, 144, 224, 239),
        ),
        splashColor: Color.fromARGB(255, 3, 4, 94),
      ),
      home: AnimatedSplashScreen(
        splash: Image.asset(
          'logo.png',
          height: 970,
          width: 865,
        ),
        backgroundColor: Colors.white,
        nextScreen: Home(),
        splashTransition: SplashTransition.scaleTransition,
        duration: 2000,
      ),
    );
  }
}
