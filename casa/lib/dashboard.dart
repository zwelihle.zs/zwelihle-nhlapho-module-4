import 'package:flutter/material.dart';
import 'package:casa/home.dart';
import 'package:casa/feature.dart';
import 'package:casa/profile.dart';

void main() => runApp(MaterialApp(
      home: Dashboard(),
    ));

class Dashboard extends StatelessWidget {
  Dashboard({Key? key}) : super(key: key);
  double matButtonH = 115.00;
  double matButtonW = 185.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // backgroundColor: const Color.fromARGB(255, 0, 120, 183),
          title: const Text('Dashboard'),
          centerTitle: true,
        ),
        body: Center(
          child: Column(children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(20.0),
                  child: MaterialButton(
                    height: 100.0,
                    minWidth: 150.0,
                    color: const Color.fromARGB(255, 0, 180, 218),
                    // textColor: Colors.white,
                    child: const Text('Profile'),
                    onPressed: () => {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Profile()))
                    },
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(20.0),
                  child: MaterialButton(
                    height: 100.0,
                    minWidth: 150.0,
                    color: const Color.fromARGB(255, 0, 180, 218),
                    // textColor: Colors.white,
                    child: const Text('Home'),
                    onPressed: () => {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Feature()))
                    },
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  padding: const EdgeInsets.all(20.0),
                  child: MaterialButton(
                    height: 100.0,
                    minWidth: 150.0,
                    color: const Color.fromARGB(255, 0, 180, 218),
                    // textColor: Colors.white,
                    child: const Text('About'),
                    onPressed: () => {},
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(20.0),
                  child: MaterialButton(
                    height: 100.0,
                    minWidth: 150.0,
                    color: const Color.fromARGB(255, 0, 180, 218),
                    // textColor: Colors.white,
                    child: const Text('Log Out'),
                    onPressed: () => {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Home()))
                    },
                  ),
                ),
              ],
            ),
          ]),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const Profile()))
          },
          child: const Icon(Icons.edit),
        ));
  }
}
