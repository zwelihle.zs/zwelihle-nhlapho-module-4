import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      home: Feature(),
    ));

class Feature extends StatelessWidget {
  Feature({Key? key}) : super(key: key);
  double boxwidth = 210;
  double boxheight = 179;
  double imgWidth = 160;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // backgroundColor: const Color.fromARGB(255, 20, 131, 199),
        title: const Text('Feature'),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  'logo.png',
                  width: 170.0,
                ),
                Image.asset(
                  'man.png',
                  width: 60.0,
                ),
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              'Accommodations',
              style: TextStyle(
                // color: Color.fromARGB(700, 0, 120, 183),
                fontSize: 20.0,
              ),
              textAlign: TextAlign.start,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Wrap(
                spacing: 22.0,
                runSpacing: 22.0,
                children: [
                  SizedBox(
                    width: boxwidth,
                    height: boxheight,
                    child: Card(
                      color: Colors.white,
                      elevation: 5.0,
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(7.0),
                          child: Column(
                            children: [
                              Image.asset(
                                'img1.jpg',
                                width: imgWidth,
                              ),
                              const SizedBox(
                                  // height: 9.0,
                                  ),
                              const Text(
                                'Hotel Mongruvia', //hotel1
                                style: TextStyle(
                                    // color: Color.fromARGB(700, 0, 120, 183),
                                    fontSize: 18.0),
                              ),
                              const SizedBox(
                                  // height: 5.0,
                                  ),
                              const Text(
                                'From R204',
                                style: TextStyle(
                                    // color: Color.fromARGB(700, 0, 150, 183),
                                    fontWeight: FontWeight.w100),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: boxwidth,
                    height: boxheight,
                    child: Card(
                      // color: const Color.fromARGB(255, 255, 255, 255),
                      elevation: 5.0,
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(7.0),
                          child: Column(
                            children: [
                              Image.asset(
                                'img2.jpg',
                                width: imgWidth,
                              ),
                              const SizedBox(
                                  // height: 9.0,

                                  ),
                              const Text(
                                'Hotel Serio', //hotel2
                                style: TextStyle(
                                    // color: Color.fromARGB(700, 0, 120, 183),
                                    fontSize: 18.0),
                              ),
                              const SizedBox(
                                  // height: 5.0,
                                  ),
                              const Text(
                                'From R231',
                                style: TextStyle(
                                    // color: Color.fromARGB(700, 0, 150, 183),
                                    fontWeight: FontWeight.w100),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: boxwidth,
                    height: boxheight,
                    child: Card(
                      // color: const Color.fromARGB(255, 255, 255, 255),
                      elevation: 5.0,
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(7.0),
                          child: Column(
                            children: [
                              Image.asset(
                                'img3.jpg',
                                width: imgWidth,
                              ),
                              const SizedBox(
                                  // height: 9.0,
                                  ),
                              const Text(
                                'Hotel Pelinski', //hotel3
                                style: TextStyle(
                                    // color: Color.fromARGB(700, 0, 120, 183),
                                    fontSize: 18.0),
                              ),
                              const SizedBox(
                                  // height: 5.0,
                                  ),
                              const Text(
                                'From R315',
                                style: TextStyle(
                                    // color: Color.fromARGB(700, 0, 150, 183),
                                    fontWeight: FontWeight.w100),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: boxwidth,
                    height: boxheight,
                    child: Card(
                      // color: const Color.fromARGB(255, 255, 255, 255),
                      elevation: 5.0,
                      child: Center(
                        child: Padding(
                          padding: const EdgeInsets.all(7.0),
                          child: Column(
                            children: [
                              Image.asset(
                                'img4.jpg',
                                width: imgWidth,
                              ),
                              const SizedBox(
                                  // height: 9.0,
                                  ),
                              const Text(
                                'Hotel Mzansi', //hotel4
                                style: TextStyle(
                                    // color: Color.fromARGB(700, 0, 120, 183),
                                    fontSize: 18.0),
                              ),
                              const SizedBox(
                                  // height: 5.0,
                                  ),
                              const Text(
                                'From R258',
                                style: TextStyle(
                                    // color: Color.fromARGB(700, 0, 150, 183),
                                    fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ]),
      ),
      // backgroundColor: Colors.white,
    );
  }
}
