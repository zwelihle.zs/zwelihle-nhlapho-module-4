import 'package:flutter/material.dart';
import 'package:casa/dashboard.dart';
import 'package:casa/signup.dart';

void main() => runApp(const MaterialApp(
      home: Login(),
    ));

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // backgroundColor: Colors.white,
        appBar: AppBar(
          // backgroundColor: const Color.fromARGB(255, 0, 120, 183),
          title: const Text('Log In'),
          centerTitle: true,
        ),
        body: Column(mainAxisSize: MainAxisSize.min, children: [
          const SizedBox(
            height: 60,
          ),
          Container(
            padding: const EdgeInsets.all(8.0),
            child: const TextField(
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Username',
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(8.0),
            child: const TextField(
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Password',
              ),
            ),
          ),
          Row(children: [
            Container(
              padding: const EdgeInsets.all(8.0),
              child: Expanded(
                child: ElevatedButton(
                    onPressed: () => {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Dashboard()))
                        },
                    child: const Text(
                      'SIGN IN',
                    )),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8.0),
              child: Expanded(
                child: ElevatedButton(
                    onPressed: () => {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => Signup()))
                        },
                    child: const Text(
                      'SIGN UP',
                    )),
              ),
            ),
          ]),
        ]));
  }
}
